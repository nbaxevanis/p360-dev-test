$(document).ready(function() {

    //setup the fileupload mechanism
    $('#fileupload').fileupload({
        url: 'classes/UploadHandler.php',
        dataType: 'json',
        done: function(e, data) {
            $('#progress').hide();
            $('.fileinput-button').hide();
            loadFilters();
            $.each(data.result.files, function(index, file) {

                $('<p/>').text('File uploaded: ' + file.name).appendTo('#files');
            });
        },
        submit: function(e, data) {
            $('#progress').show();
        },
        progressall: function(e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .bar').css(
                    'width',
                    progress + '%'
                    );
        }
    }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');

    $('#progress').hide();

    //Setup the render graph button
    $('.render-button').on('click', function() {
        if ($('#filters').val() == null) {
            alert("You must select at least one filter to display");
        } else {
            var url = 'classes/main.php?action=renderGraph&' + $('#filtersForm').serialize();
            $('#finalGraphReview').html($('<img/>').attr('src', url));
            $('#step_03').show();
        }
        return false;
    });

    //generic function that is being used to call the processor
    var callProcessor = function(data, callback) {
        $.ajax({
            type: "POST",
            url: "classes/main.php",
            data: data
        }).done(callback);
    }

    //Init function that checks if the backend system is setup correctly before starting
    var checkSystem = function() {
        callProcessor({action: "check"}, function(msg) {
            if (msg.status == 'ok') {
                $('#initialise_area').hide();
                $('#step_01').show();
            } else {
                //add button for retry
                alert("There was an error" + msg.reason);
            }
            ;
        });
    };


    //function being called when we have a success file upload
    //this function will ask for the available filters on the file that we have upload and generate the wanted multiselect list
    var loadFilters = function() {
        callProcessor({action: "getFilters"}, function(msg) {
            if (msg.status == 'ok') {
                $.each(msg.options, function(key, value) {
                    $('#filters').append($('<option></option>').val(value).html(value));
                });

                $('#filters').multiselect({
                    buttonClass: 'btn',
                    buttonWidth: 'auto',
                    buttonContainer: '<div class="btn-group" />',
                    maxHeight: false,
                    buttonText: function(options) {
                        if (options.length == 0) {
                            return 'None selected <b class="caret"></b>';
                        }
                        else if (options.length > 3) {
                            return options.length + ' selected  <b class="caret"></b>';
                        }
                        else {
                            var selected = '';
                            options.each(function() {
                                selected += $(this).text() + ', ';
                            });
                            return selected.substr(0, selected.length - 2) + ' <b class="caret"></b>';
                        }
                    }
                });
                $('#step_02').show();
            } else {
                //add button for retry
                alert("There was an error" + msg.reason);
            }
        });
    }

    //call the checkSystem on page load
    checkSystem();

});
