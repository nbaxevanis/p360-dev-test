<?php

/**
 * processor class is the main engine for the application
 * It checks to see if server is on a state to be able to work without any issues
 * and is able to parse the uploaded file and generate our graph
 */
class processor {

    const _GRAPH_WIDTH = 500;
    const _GRAPH_HEIGHT = 400;
    const _GRAPH_TITLE = 'P360 sample graph';
    const _GRAPH_DOT_COLOR = 'red';
    const _GRAPH_DOT_WIDTH = 8;
    const _FILE_STORAGE_DIR = '../uploads/';
    const _FILE_NAME = 'data.csv';

    private $criteria = array();
    private $loadedData = false;

    /**
     * This method will build a list with all the unique filter types available from the file
     *
     * @return array
     */
    private function _generateUniqueFilterOptions() {
        $data = $this->_getDataFromFile();

        $options = array();

        foreach ($data as $entry) {
            array_push($options, $entry[0]);
        }

        return array_unique($options);
    }

    /**
     * This method will parse the csv file and generate the loadedData array with the values from the file
     * if this method is already called (thus the file is already parsed) it will return the existing data
     *
     * @return array
     */
    private function _getDataFromFile() {
        if (!$this->loadedData) {
            $this->loadedData = array();
            ini_set("auto_detect_line_endings", true); //bug on mac computers :)
            if (($handle = fopen(self::_FILE_STORAGE_DIR . self::_FILE_NAME, "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 100000, ",")) !== FALSE) {
                    array_push($this->loadedData, $data);
                }
                fclose($handle);
            }
        }

        return $this->loadedData;
    }

    /**
     * This method will get an array with wanted filters and validate them agains the valid options found in the file
     * Then it will generate a unique list with the wanted filters and setup the criteria variable
     *
     * @param array $filters
     */
    public function validateAndHandleFilters(array $filters) {
        $uniqueOptions = $this->_generateUniqueFilterOptions();

        foreach ($filters as $value) {
            if (in_array($value, $uniqueOptions)) {
                array_push($this->criteria, $value);
            }
        }

        $this->criteria = array_unique($this->criteria);
    }

    /**
     * This method will speak to the graph builder in order to setup and build the graph that we want
     * at the end based on the render flag we can take the image as results or just setup the graph to make sure that everything is working alright
     *
     * @param bool $render
     */
    public function renderGraph($render = true) {

        $datax = array();
        $datay = array();

        $allData = $this->_getDataFromFile();
        foreach ($allData as $entry) {
            if (in_array($entry[0], $this->criteria)) {
                array_push($datax, (int) $entry[1]);
                array_push($datay, (int) $entry[2]);
            }
        }

        $graph = new Graph(self::_GRAPH_HEIGHT, self::_GRAPH_HEIGHT);
        $graph->SetScale("linlin");

        $graph->img->SetMargin(40, 40, 40, 40);
        $graph->SetShadow();

        $graph->title->Set(self::_GRAPH_TITLE);
        $graph->title->SetFont(FF_FONT1, FS_BOLD);

        $sp1 = new ScatterPlot($datay, $datax);

        $sp1->mark->SetType(MARK_FILLEDCIRCLE);
        $sp1->mark->SetFillColor(self::_GRAPH_DOT_COLOR);
        $sp1->mark->SetWidth(self::_GRAPH_DOT_WIDTH);

        $graph->Add($sp1);
        if ($render) {
            $graph->Stroke();
        }
    }

    /**
     * This method was made in order to validate some basic things before the we start working with the main app.
     * Some basic checks that we can inform user to solve before moving on
     *
     * @return json response
     */
    public function checkSystem() {
        $error = 'Unknown';
        $status = 'error';
        if (!file_exists(self::_FILE_STORAGE_DIR)) {
            $error = 'Upload directory does not exist. Please create a directory named `' . self::_FILE_STORAGE_DIR . '`';
        } else {
            if (!is_writable(self::_FILE_STORAGE_DIR)) {
                $error = 'Upload directory is not writable. Please make sure that we can write into this directory `' . self::_FILE_STORAGE_DIR . '`';
            } else {
                //lets create a demo file just to make sure that everything is ok
                $saved = file_put_contents(self::_FILE_STORAGE_DIR . self::_FILE_NAME, '"type1",1,2' . PHP_EOL . '"type2",3,4');
                if (!$saved) {
                    $error = 'There was an unknown error while trying to create a file into this directory `' . self::_FILE_STORAGE_DIR . '`';
                } else {
                    //lets make a final check that we can generate a graph without any issues
                    $this->criteria = array('type1', 'type2');
                    try {
                        $this->renderGraph(false);
                        $status = 'ok';
                        $error = false;
                        unlink(self::_FILE_STORAGE_DIR . self::_FILE_NAME);
                    } catch (Exception $e) {
                        $error = 'There was an error trying to create the graph';
                    }
                }
            }
        }

        header('Content-type: application/json');
        echo json_encode(array('status' => $status, 'reason' => $error));
    }

    /**
     * This method will return a json response with all the available filters found in the uploaded file
     *
     * @return json response
     */
    public function getFilters() {

        $uniqueOptions = $this->_generateUniqueFilterOptions();
        header('Content-type: application/json');
        echo json_encode(array('status' => 'ok', 'options' => $uniqueOptions));
    }

}

