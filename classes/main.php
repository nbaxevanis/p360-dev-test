<?php

//load the libraries
require_once ('../libraries/jpgraph/jpgraph.php');
require_once ('../libraries/jpgraph/jpgraph_scatter.php');
require_once ('processor.php');

//create a new processor
$processor = new processor();

//get the wantedAction if any, or use default value check
$wantedAction = array_key_exists('action', $_REQUEST) ? $_REQUEST['action'] : ' check';

//call processor method based on the action
switch ($wantedAction) {
    case "check":
        $processor->checkSystem();
        break;
    case "getFilters":
        $processor->getFilters();
        break;
    case "renderGraph":
        if (array_key_exists('filters', $_REQUEST)) {
            $processor->validateAndHandleFilters($_REQUEST['filters']);
        }
        $processor->renderGraph();
        break;
}
