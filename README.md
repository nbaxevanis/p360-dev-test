P360 development test
========================

This is the repository for the P369 dev test
In this repository you can find all the files needed for the app to work 100% correct.
JPGraph library is included in the repository and it is located inside libraries folder.


1) Installing
----------------------------------

### Clone the repository

The fastest installation is using the clone command in order to clone the repository

    git clone https://nbaxevanis@bitbucket.org/nbaxevanis/p360-dev-test.git

### Download the source code

If for some reason you cannot clone the repository, then you can always download the source code by going into the downloads section
select branches and there you will be able to see master branch. At the end of the line you can select the ziped type you want to download the package


2) Running
----------------------------------

Once downloaded and uploaded to the server you can call index.html in order for the application to run


3) Notes
----------------------------------

 - Inside the package I have added a sample csv that I used during development with 300 entries
 - While selecting the file for upload only .csv files can be uploaded